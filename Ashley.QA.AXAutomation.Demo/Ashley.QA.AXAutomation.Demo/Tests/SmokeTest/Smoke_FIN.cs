﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Ashley.QA.Automation.Framework.Windows;
using Ashley.QA.Automation.Framework.UIAutomationControl;
using Ashley.QA.Dynamics.AX2012.Automation.Utils;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Ashley.QA.Automation.Framework.Web;
using OpenQA.Selenium;
using Ashley.QA.Sitecore.Automation.SaleOrder;
using Microsoft.VisualStudio.TestTools.UITest.Common.UIMap;
using System.IO;
using Ashley.QA.Automation.Web.Framework.Enum;
using System.Data;
using System.Reflection;

namespace Ashley.QA.Dynamics.AX.Automation.Tests.SmokeTests
{
    [CodedUITest]
    [DeploymentItem(@"Utils\TestDriver", @"Utils\TestDriver")]
    [DeploymentItem(@"TestData", @"TestData")]
    public class Smoke_FIN
    {
        public Smoke_FIN()
        {

        }

       


        


        [TestMethod]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(62922)]
        [Description("Verify the publish to item group functionality")]
        public void VerifyThePublishToItemGroupFunctionality()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = "ECM/Inventory management/Setup/Inventory/Ashley sales category/Sales category";
            string FinalSalesCategory = "final sales category";
            string SalesCategoryToFinalMapping = "Sales category to final category mapping";
            string AshleyParameters = "Inventory and warehouse management parameters";
            string ItemGroups = "Inventory/Item groups";
            String guid = Guid.NewGuid().ToString();
            string[] testValue = guid.Split('-');
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SC = new WinWindow();
            WinWindow wnd_FinalSalesCategory = new WinWindow();
            WinWindow wnd_InventoryAndWarehouseParameters = new WinWindow();
            WinWindow wnd_SalesToFinalSalesMapping = new WinWindow();
            WinWindow wnd_ItemGroups = new WinWindow();
            WinEdit edit_SalesCategory = new WinEdit();
            WinEdit edit_Name = new WinEdit();
            WinButton btn_Ok = new WinButton();
            WinButton btn_Close = new WinButton();
            WinTreeItem treeItem = new WinTreeItem();
            WinMenuItem menu_New = new WinMenuItem();
            WinHyperlink hyperlink_AshleyParameter = new WinHyperlink();
            WinCheckBox checkbox_EnableFinalCategory = new WinCheckBox();
            WinTable table_itemGroup = new WinTable();
            UITestControlCollection uitcc_ItemGroup = new UITestControlCollection();
            UITestControlCollection uitcc_DefaultAccounts = new UITestControlCollection();
            #endregion

            #region Test Steps
            // 1, AX 2012 Sign on
            // 1.1, Launch AX 2012 environment
            // 1.2, Put a valid username/password and hit enter
            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);

            // 2, Go to Inventory management->Setup->Inventory->Ashley sales category->sales category
            wnd_AX.NavigateTo(AddressPath);

            // 3, Click on New sales category icon
            wnd_AX.ClickRibbonMenuItem("Sales category", "New", "Sales category", true);

            // 4, Give the @name and @salescategory and click ok
            wnd_SC = UITestControl.Desktop.FindWinWindow(new { Name = "‪Create sales category‬ (‎‪1‬ - ‎‪ecm‬)‎‪ :‬‬‬‬", ClassName = "AxTopLevelFrame" });

            edit_SalesCategory = wnd_SC.DetectControl<WinEdit>(new { Name = "Sales category" });
            edit_SalesCategory.EnterWinText(testValue[0]);
            edit_Name = wnd_SC.DetectControl<WinEdit>(new { Name = "Name" });
            edit_Name.EnterWinText(testValue[0]);

            btn_Ok = wnd_SC.DetectControl<WinButton>(new { Name = "Ok" });
            btn_Ok.WinClick();

            // 5, Go to Inventory management->Setup->Inventory->Ashley sales category->final sales category
            wnd_AX.SelectFromTreeItems(FinalSalesCategory);

            // 6, Click on New button in the top            
            wnd_FinalSalesCategory = UITestControl.Desktop.DetectControl<WinWindow>(new { Name = "‪Final sales category‬(‎‪1‬ - ‎‪ecm‬)‎", ClassName = "AxTopLevelFrame" });
            menu_New = wnd_FinalSalesCategory.DetectControl<WinClient>(new { Name = "MiddleRow" }).DetectControl<WinGroup>(new { Name = "NewDeleteGroup" }).DetectControl<WinMenuItem>(new { Name = "New" });
            Mouse.Click(menu_New);

            edit_SalesCategory = wnd_FinalSalesCategory.DetectControl<WinWindow>(new { ClassName = "AxEdit" }).DetectControl<WinEdit>(new { Name = "Final sales category" });
            edit_SalesCategory.EnterWinTextV2(testValue[1]);

            edit_Name = wnd_FinalSalesCategory.DetectControl<WinEdit>(new { Name = "Name" });
            edit_Name.EnterWinTextV2(testValue[1]);
            //edit_Name.Text = testValue[1];

            wnd_FinalSalesCategory.CloseWindow();

            // 12, Verify the enable/disable of publish to item group button
            wnd_AX.SelectFromTreeItems(AshleyParameters);
            wnd_InventoryAndWarehouseParameters = UITestControl.Desktop.FindWinWindow(new { Name = "‪Inventory and warehouse management parameters‬ :", ClassName = "AxTopLevelFrame" });
            hyperlink_AshleyParameter = wnd_InventoryAndWarehouseParameters.DetectControl<WinHyperlink>(new { Name = "Ashley parameters" });
            hyperlink_AshleyParameter.WinClick();
            checkbox_EnableFinalCategory = wnd_InventoryAndWarehouseParameters.DetectControl<WinCheckBox>(new { Name = "Enable final category" });
            if (checkbox_EnableFinalCategory.Checked == true)
            {
                checkbox_EnableFinalCategory.WinClick();
            }
            wnd_InventoryAndWarehouseParameters.CloseWindow();

            wnd_AX.SelectFromTreeItems(SalesCategoryToFinalMapping);
            wnd_SalesToFinalSalesMapping = UITestControl.Desktop.FindWinWindow(new { Name = "‪Sales category to final category mapping‬ (‎‪1‬ - ‎‪ecm‬)‎", ClassName = "AxTopLevelFrame" });
            Assert.IsNotNull(wnd_SalesToFinalSalesMapping.FindRibbonMenuItem("Overview", "Publish", "Publish all sales categories to item groups"), "Icon Enabled");
            wnd_SalesToFinalSalesMapping.CloseWindow();

            wnd_AX.SelectFromTreeItems(AshleyParameters);
            wnd_InventoryAndWarehouseParameters = UITestControl.Desktop.FindWinWindow(new { Name = "‪Inventory and warehouse management parameters‬ :", ClassName = "AxTopLevelFrame" });
            hyperlink_AshleyParameter = wnd_InventoryAndWarehouseParameters.DetectControl<WinHyperlink>(new { Name = "Ashley parameters" });
            hyperlink_AshleyParameter.WinClick();
            checkbox_EnableFinalCategory = wnd_InventoryAndWarehouseParameters.DetectControl<WinCheckBox>(new { Name = "Enable final category" });
            if (checkbox_EnableFinalCategory.Checked == false)
            {
                checkbox_EnableFinalCategory.WinClick();
            }

            wnd_InventoryAndWarehouseParameters.CloseWindow();

            // 7, Go to Inventory management->Setup->Inventory->Ashley sales category->Sales category to Final category mapping form
            wnd_AX.SelectFromTreeItems(SalesCategoryToFinalMapping);
            wnd_SalesToFinalSalesMapping = UITestControl.Desktop.FindWinWindow(new { Name = "‪Sales category to final category mapping‬ (‎‪1‬ - ‎‪ecm‬)‎", ClassName = "AxTopLevelFrame" });

            // 8, Click Create mapping Link
            // 9, click the sales category drop down and select step 4 created sales category and click final sales category dropdown and select final sales category which was created in step 6.                     
            wnd_SalesToFinalSalesMapping.ClickRibbonMenuItem("ActionPaneTab", "Maintain", "Create mapping");

            edit_SalesCategory = wnd_SalesToFinalSalesMapping.DetectControl<WinEdit>(new { Name = "Sales category.Sales category" });
            edit_SalesCategory.EnterWinText(testValue[0]);
            Keyboard.SendKeys("{TAB}");
            Keyboard.SendKeys(testValue[0]);
            Keyboard.SendKeys("{TAB}");
            Keyboard.SendKeys(testValue[1]);

            // 10, Check for the button Publish to item group
            // 11, Click on publish all sales categories to item groups button
            wnd_SalesToFinalSalesMapping.ClickRibbonMenuItem("ActionPaneTab", "Publish", "Publish all sales categories to item groups");
            DynamicsAxCommonFunctions.HandleInfolog(true);

            wnd_AX.SelectFromTreeItems(ItemGroups);
            wnd_ItemGroups = UITestControl.Desktop.FindWinWindow(new { Name = "‪Item groups‬ (‎‪1‬ - ‎‪ecm‬)‎", ClassName = "AxTopLevelFrame" });
            uitcc_ItemGroup = wnd_ItemGroups.GetAllRecords("Grid");
            uitcc_ItemGroup.RemoveAll(x => !x.GetProperty("Value").ToString().Contains(testValue[0]));
            if (uitcc_ItemGroup.Count == 0)
            {
                throw new Exception("Item group not found");
            }
            wnd_ItemGroups.CloseWindow();
            #endregion

            #region Test Cleanup
            #endregion
        }

       

        [TestMethod]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(91871)]
        [Description("Finance EOD Reports")]
        public void FinanceEodReports()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath_AshleyReport = "ECM/Accounts receivable/Reports/Ashley reports";
            string ARbalancing = "Accounts receivable/Reports/Ashley reports/AR Balancing";
            string ItemSalesDetails = "Accounts receivable/Reports/Ashley reports/Item Sales Detail";
            string CashReceiptJournalDetails = "Accounts receivable/Reports/Ashley reports/Cash receipt journal";
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;


            #endregion

            #region Local variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_CreateCredit = new WinWindow();
            WinWindow wnd_Infolog = new WinWindow();
            WinWindow wnd_ISD = new WinWindow();
            WinWindow wnd_CRJ = new WinWindow();
            WinWindow wnd_ARBalancing = new WinWindow();
            HtmlRow row_Record = new HtmlRow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_discountPercentage = new WinEdit();
            WinButton btn_OK = new WinButton();

            WinButton btn_Select = new WinButton();
            WinButton btn_Ok = new WinButton();
            WinEdit edit_FromDate = new WinEdit();
            WinEdit edit_ToDate = new WinEdit();
            WinEdit edit_RetailChannel = new WinEdit();
            WinEdit edit_RetailType = new WinEdit();
            WinCheckBox checkBox_ShowTotal = new WinCheckBox();
            WinEdit edit_CustomerAccount = new WinEdit();

            #endregion

            #region Test Steps

            // 1) AX 2012 Sign on
            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);

            // 2) Navigate to ECM->Accounts Receivables->Reports->Ashley Reports->Item sales Detail
            wnd_AX.NavigateTo(AddressPath_AshleyReport);
            wnd_AX.SelectFromTreeItems(ItemSalesDetails);

            // 3) Verify the @Filters present in the Report Screen
            // 4) Enter the valid filter criteria and generate the report
            wnd_ISD = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "‪Item Sales Detail" }, PropertyExpressionOperator.Contains);
            edit_FromDate = wnd_ISD.DetectControl<WinEdit>(new { Name = "From date" });
            edit_FromDate.Text = DateTime.Now.AddDays(-2).ToString("dd/MM/yyyy");

            edit_ToDate = wnd_ISD.DetectControl<WinEdit>(new { Name = "To date" });
            edit_ToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            btn_Ok = wnd_ISD.DetectControl<WinButton>(new { Name = "OK" });
            btn_Ok.WinClick();

            while (true)
            {
                try
                {
                    wnd_Infolog = UITestControl.Desktop.FindWinWindow(new { Name = "Infolog", ClassName = "AxTopLevelFrame" }, PropertyExpressionOperator.Contains);

                    if (wnd_Infolog.Exists)
                    { break; }
                }
                catch (Exception ex) { }
                wnd_ISD = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "‪Item Sales Detail" }, PropertyExpressionOperator.Contains);
                try {
                    WinToolBar toolBar = wnd_ISD.DetectControl<WinToolBar>(new { Name = "Toolstrip" });
                    if (toolBar.BoundingRectangle.Height > 0)
                    {
                        break;
                    }
                } catch (Exception ex) { }
            }

            wnd_ISD.CloseWindow();

            // 5) Navigate to ECM->Accounts Receivables->Reports->Ashley Reports->Cash receipt Journal report
            // 6) Enter the valid filter criteria and generate the report
            wnd_AX.NavigateTo(AddressPath_AshleyReport);
            wnd_AX.SelectFromTreeItems(CashReceiptJournalDetails);
            wnd_CRJ = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "Cash receipt journal" }, PropertyExpressionOperator.Contains);


            edit_FromDate = wnd_CRJ.DetectControl<WinEdit>(new { Name = "From date" });
            edit_FromDate.Text = DateTime.Now.AddMonths(-1).ToString("dd/MM/yyyy");

            edit_ToDate = wnd_CRJ.DetectControl<WinEdit>(new { Name = "To date" });
            edit_ToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            btn_Ok = wnd_CRJ.DetectControl<WinButton>(new { Name = "OK" });
            btn_Ok.WinClick();

            wnd_CRJ = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "‪Cash receipt journal‬" }, PropertyExpressionOperator.Contains);
            wnd_CRJ.CloseWindow();

            // 7) Navigate to ECM->Accounts Receivables->Reports->Ashley Reports->AR balancing report
            // 8) Enter the valid filter criteria and generate the report
            wnd_AX.NavigateTo(AddressPath_AshleyReport);
            wnd_AX.SelectFromTreeItems(ARbalancing);

            wnd_ARBalancing = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "‪AR Balancing" }, PropertyExpressionOperator.Contains);

            edit_FromDate = wnd_ARBalancing.DetectControl<WinEdit>(new { Name = "From date" });
            edit_FromDate.Text = DateTime.Now.AddMonths(-1).ToString("dd/MM/yyyy");

            edit_ToDate = wnd_ARBalancing.DetectControl<WinEdit>(new { Name = "To date" });
            edit_ToDate.Text = DateTime.Now.ToString("dd/MM/yyyy");

            edit_RetailType = wnd_ARBalancing.DetectControl<WinEdit>(new { Name = "Report type" });
            edit_RetailType.Text = "Detail";

            checkBox_ShowTotal = wnd_ARBalancing.DetectControl<WinCheckBox>(new { Name = "Show total" });
            if (checkBox_ShowTotal.Checked == false)
            {
                checkBox_ShowTotal.WinClick();
            }

            btn_Ok = wnd_ARBalancing.DetectControl<WinButton>(new { Name = "OK" });
            btn_Ok.WinClick();

            //  Verify the data in the AR balancing report
            wnd_ARBalancing = UIAutomationControl.Desktop.DetectControl<WinWindow>(new { Name = "‪AR Balancing" }, PropertyExpressionOperator.Contains);
            wnd_ARBalancing.CloseWindow();


            #endregion

            #region Test Cleanup

            #endregion
        }

       

        [TestMethod]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(107000)]
        [Description("FIN - Fraud report")]
        public void FinFraudReport()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string salesOrder = "ECM/Sales and Marketing/Reports/Sales orders";
            string OpenSaleOrderWithAddress = "Sales and Marketing/Reports/Sales orders/Open sales orders with addresses";

            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_SC = new WinWindow();
            WinWindow wnd_OpenSaleOrderWithAddress = new WinWindow();
            WinWindow wnd_‪afiCustSalesOpenOrders = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinButton btn_Select = new WinButton();
            WinButton btn_Ok = new WinButton();
            WinEdit edit_CreationDate = new WinEdit();
            string creation_Date = DateTime.Today.ToString("dd-MM-yyyy");
            DateTime creationDate = Convert.ToDateTime(creation_Date);
            HtmlRow row_Record = new HtmlRow();
            Dictionary<String, String> dict_ColumnAndValues = new Dictionary<String, String>();
            #endregion


            #region Test Steps
            // 1, Login to AX

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);

            // 2, Click on the Sales and Marketing -> Reports -> sales orders->Open Sales Order with addresses 
            wnd_AX.NavigateTo(salesOrder);
            wnd_AX.SelectFromTreeItems(OpenSaleOrderWithAddress);

            // 3, click ->Select button and choose Created date from the Calendar and Click ok 
            wnd_OpenSaleOrderWithAddress = UITestControl.Desktop.FindWinWindow(new { Name = "‪Open sales orders with addresses‬ (‎‪1‬)‎", ClassName = "AxTopLevelFrame" });
            btn_Select = wnd_OpenSaleOrderWithAddress.DetectControl<WinButton>(new { Name = "Select" });
            btn_Select.WinClick();

            wnd_afiCustSalesOpenOrders = UITestControl.Desktop.FindWinWindow(new { Name = "‪afiCustSalesOpenOrders‬ (‎‪1‬ - ‎‪ecm‬)‎", ClassName = "AxPopupFrame" });
            dict_ColumnAndValues = new Dictionary<string, string>();
            dict_ColumnAndValues.Add("Field", "Creation date");
            wnd_afiCustSalesOpenOrders.EnterValueToCell("RangeGrid", dict_ColumnAndValues, "Criteria", creation_Date);
            btn_Ok = wnd_afiCustSalesOpenOrders.DetectControl<WinButton>(new { Name = "OK" });
            btn_Ok.WinClick();

            wnd_OpenSaleOrderWithAddress = UITestControl.Desktop.FindWinWindow(new { Name = "‪Open sales orders with addresses‬ (‎‪1‬)‎", ClassName = "AxTopLevelFrame" });
            edit_CreationDate = wnd_OpenSaleOrderWithAddress.DetectControl<WinEdit>(new { Name = "Creation date" });
            string creationDateText = edit_CreationDate.Text;
            string[] creationDateTime = creationDateText.Split('.');
            Assert.AreEqual(creationDate.AddDays(-1).AddHours(18).AddMinutes(00).AddSeconds(00).ToString("dd-MMM-yyyy hh:mm:ss tt").ProcessString(), creationDateTime[0].ProcessString(), "Time Mismatch");
            Assert.AreEqual(creationDate.AddHours(17).AddMinutes(59).AddSeconds(59).ToString("dd-MMM-yyyy hh:mm:ss tt").ProcessString(), creationDateTime[2].ProcessString(), "Time Mismatch");
            btn_Ok = wnd_OpenSaleOrderWithAddress.DetectControl<WinButton>(new { Name = "OK" });
            btn_Ok.WinClick();

            // 4, Click OK button in Open sales order with address pop -up

            wnd_OpenSaleOrderWithAddress = UITestControl.Desktop.FindWinWindow(new { Name = "‪Open sales orders with addresses‬ (‎‪1‬)‎", ClassName = "AxTopLevelFrame" });
            wnd_OpenSaleOrderWithAddress.CloseWindow();

            #endregion

            #region Test Cleanup

            #endregion
        }
   


        #region Additional test attributes

        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestContext.CustomTestCleanup();
        }

        #endregion

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
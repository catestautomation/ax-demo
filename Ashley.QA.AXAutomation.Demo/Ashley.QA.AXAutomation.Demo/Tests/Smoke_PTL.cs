﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Ashley.QA.Automation.Framework.Windows;
using Ashley.QA.Automation.Framework.UIAutomationControl;
using Ashley.QA.Dynamics.AX2012.Automation.Utils;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Ashley.QA.Automation.Framework.Web;
using OpenQA.Selenium;

using System.Data;

namespace Ashley.QA.Dynamics.AX.Automation.Tests.SmokeTests
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    [DeploymentItem(@"Utils\TestDriver", @"Utils\TestDriver")]
    [DeploymentItem(@"TestData", @"TestData")]
    public class Smoke_PTL
    {    
        public Smoke_PTL()
        {
        }

        #region Global Variables
        
        public static string saleOrder;
     

        #endregion

      

        //[TestMethod]
        [TestCategory("Portal")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(91869)]
        [Description("PTL Smoke: Verify the PO Confirmation, Invoicing, Queue Movement")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "91869", DataAccessMethod.Sequential)]
        public void PTLSmoke_VerifyThePOConfirmation_Invoicing_QueueMovement()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;          
           
            string PurchaseOrder = string.Empty;
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            IWebDriver driver = null;
            #endregion

            #region Test Steps

            #region Pre-requisite
            saleOrder = "725000144598";
           PurchaseOrder = saleOrder + 1.ToString("00");
            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);            
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });

            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");


            // 4.2, Seach and click on the purchase order
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
             
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            // 4.3, Enter the @POERPnumber & @POConfirmeddeliverydate greater than Latest estimated delivery date
            // Click Apply to lines         
            // 4.4, Select the Reason Code from dropdown and click OK
            // Click Save and Close
            String ERPNumber = PurchaseOrder.Substring(PurchaseOrder.Length - 5);
            driver.EnterERPNumberAndConfirmedDeliveryDate(ERPNumber, "Purchase order", "Homestore data");
                   

            // 8, Invoice the HD Purchase Order(s) via Homestore Portal (PTL)
            // 8.1, Click on the Scheduled cue icon
            driver.SelectFromRibbon("Purchase order", "Stages", "Scheduled");


            // 8.2, Click on the Scheduled cue icon
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            //driver.ClickACellinTable("Purchase order", PurchaseOrder);
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            // 8.3, Click Invoice Process => New Vendor Invoice => Post Invoice
            driver.Invoice();

            #endregion

            #region Test Cleanup
            #endregion

        }

        //[TestMethod]
        [TestCategory("Portal")]
        [WorkItem(116305)]
        [Description("PTL AXFC72: Verify the Field Retail Sales associate Indicator(RSA) is  Present in the Purchase Order Header UI Validation")]
        public void PTLVerifyTheFieldRetailSalesAssociateIndicatorIsPresentInThePurchaseOrderHeaderUIValidation()
        {
            #region Test Data
            string Path = AX.Automation.Properties.Settings.Default.AX_Path;
            string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;
            string Path_AccountsPayable = "ECM/Accounts payable/Common/Purchase orders/All purchase orders";
            string portal_url = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.Portal_URL;
            string portal_Username = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalUserName;
            string portal_Password = Ashley.QA.Dynamics.AX.Automation.Properties.Settings.Default.PortalPassword;
            // string saleOrder;
            #endregion

            #region Local Variables
            WinWindow wnd_AX = new WinWindow();
            WinWindow wnd_SO = new WinWindow();
            WinWindow wnd_URQ = new WinWindow();
            UITestControlCollection uitcc_SOLines = new UITestControlCollection();
            WinEdit edit_DeliverRemainder = new WinEdit();
            WinButton btn_Ok = new WinButton();
            WinWindow wnd_PopUp = new WinWindow();
            WinButton btn_Yes = new WinButton();
            IWebDriver driver = null;
            string ERPNumber = string.Empty;
             List<string> list_output = new List<string>();
            List<String> list_InfologMessages = new List<String>();
            Dictionary<string, bool> dic_output = new Dictionary<string, bool>();
            Dictionary<string, string> dic_POHeaderInputValues = new Dictionary<string, string>();
            #endregion

            #region Pre-requisite

            //  1, Create Sales Order using credit card payment type
            //  1.1, Create SO with @ItemCode, @Quantity,@RSA-ID, @Address1, @City, @State, @Zipcode, @CardNumber, @ExpiryMonth, @ExpiryYear, @SecurityCode using the Browser, @SitecoreURL and note down the @SaleOrder and @PurchaseOrder
            saleOrder = "725000143880";


            string PurchaseOrder = saleOrder + 1.ToString("00");

            #endregion

            wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
            wnd_AX.NavigateTo(AddressPath);
            wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
            wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);
            wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
            wnd_SO.WaitForPurchaseOrderToBeGenerated();
            driver = PortalCommonFunctionsSelenium.LaunchPortal(portal_url, portal_Username, portal_Password);
            driver = driver.SelectTab("Homestore portal");
            driver.SelectFromRibbon("Purchase order", "Stages", "New");
            driver = driver.PortalFilter("Purchase order", PurchaseOrder);
            
            driver.SelectFromRibbon("Purchase order", "Maintain", "Edit");

            dic_POHeaderInputValues.Add("RSAIndicator:", "ToVerifyHeaderAlone");
            dic_output = driver.VerifyPOHeaderValues("Purchase order", "References", dic_POHeaderInputValues, true);

            foreach (KeyValuePair<string, bool> keyvaluePair in dic_output)
            {
                if (keyvaluePair.Value == false)
                {
                    list_output.Add(keyvaluePair.Key);
                }
            }

            if (list_output.Count > 0)
            {
                Assert.Fail("Given Key is not present inside the Group", list_output);
            }

        }


        #region Additional test attributes

        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestContext.CustomTestCleanup();
        }

        #endregion

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
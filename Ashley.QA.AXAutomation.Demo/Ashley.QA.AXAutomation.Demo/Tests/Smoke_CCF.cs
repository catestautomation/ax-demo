﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
using Ashley.QA.Automation.Framework.Windows;
using Ashley.QA.Automation.Framework.UIAutomationControl;
using Ashley.QA.Dynamics.AX2012.Automation.Utils;
using System.Text.RegularExpressions;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using Ashley.QA.Automation.Framework.Web;
using OpenQA.Selenium;
using System.Threading;
using System.Diagnostics;
using System.IO;
using System.Data;

namespace Ashley.QA.Dynamics.AX.Automation.Tests.SmokeTests
{
    [CodedUITest]
    [DeploymentItem(@"Utils\TestDriver", @"Utils\TestDriver")]
    [DeploymentItem(@"TestData", @"TestData")]
    public class Smoke_CCF
    {
        public Smoke_CCF()
        {
        }
  
        public static string saleOrder;
        public string productCode;
        public string cardType;
        WinWindow wnd_AX = new WinWindow();
        public bool IsInvoiceNeeded = false;

      

       

        //[TestMethod]
        [TestCategory("CCF")]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(91888)]
        [Description("Apply Discounts - Header level / Line level")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "91888", DataAccessMethod.Sequential)]
        public void ApplyDiscountsHeaderLevelLineLevel()
        {
            int iterationCount = TestContext.DataRow.Table.Rows.IndexOf(TestContext.DataRow);
            if (iterationCount == 0)
            {
                #region Test Data

                string PurchaseOrder = String.Empty;
                string LineLevelDiscountAmount = TestContext.DataRow["LLDA"].ToString();
                string LineLevelDiscountReasonCode = "Third Party Vendor";//TestContext.DataRow["LLDAReasoncode"].ToString();
                string HeaderLevelDiscountPercentage = TestContext.DataRow["HLDP"].ToString();
                string HeaderLevelDiscountReasonCode = TestContext.DataRow["HLDPReasoncode"].ToString();

                string Path = AX.Automation.Properties.Settings.Default.AX_Path;
                string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;

                #endregion

                #region Local Variables
                WinWindow wnd_SO = new WinWindow();
                UITestControlCollection uitcc_SOLines = new UITestControlCollection();
                WinWindow wnd_SODiscount = new WinWindow();
                WinEdit edit_DicountAmount = new WinEdit();
                WinButton btn_ApplyAndClose = new WinButton();
                WinWindow wnd_DH = new WinWindow();
                UITestControlCollection uitcc_DHLines = new UITestControlCollection();

                List<String> list_InfologMessages = new List<String>();
                List<String> list_ExpectedDiscountMessages = new List<String>();

                #endregion

                #region Pre-requisite

                //  1, Use the Sales order from Testcase #121476
                //     CreateASaleOrderInSitecoreCCF();
                saleOrder = "725000144596";
               
                #endregion

                #region Test Steps

                //  2, Go to AX application - Sale Order form
                wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
                wnd_AX.NavigateTo(AddressPath);
                wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);
                wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);

                //  3, Apply line level discount in $ (positive) for the sales order
                //  3.1, Go to Sales order tab and Click on Modify in action pane
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Modify");
                Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Complete"), "Sale Order is not editable.");

                //  3.2, Select the main line and Click Add line discount (right side drop down)
                //  3.3, Enter @LineLevelDisountAmount in Manual Disount $, Select the @LineleveldiscountReasoncode, Click on Apply and Close
                //  3.4, Click Close in infolog Infolog should get closed
                //  3.5, Note: Repeat from Step 2 to Step 4 for all the main lines available in the selected Sales order form
                //wnd_SO.ApplyColumnFilter("SalesLineGrid", "Line stage", "Is not", "Delivery charge");
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery charge"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String itemNumber = row_SOLine.GetColumnValueOfARecord("Item number");
                    Double origQty = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Orig qty"));
                    Double bd_NetAmount = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Net amount"));
                    Double discountAmount = (Convert.ToDouble(LineLevelDiscountAmount) / origQty);

                    row_SOLine.SelectARecord(true, false);
                    wnd_SO.ClickTableMenuItem("LineOverviewActionTab", "Add line discount", null);

                    wnd_SODiscount = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order discount:", ClassName = "AxTopLevelFrame" });
                    edit_DicountAmount = wnd_SODiscount.DetectControl<WinEdit>(new { Name = "Manual discount $" });
                    edit_DicountAmount.EnterWinTextV2(LineLevelDiscountAmount);
                    wnd_SODiscount.SelectValueFromCustomDropdown("Reason code", "Default comment", LineLevelDiscountReasonCode);
                    btn_ApplyAndClose = wnd_SODiscount.DetectControl<WinButton>(new { Name = "Apply and Close" });
                    btn_ApplyAndClose.WinClick();
                    list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
                    Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

                    try
                    {
                        String discountMessage = list_InfologMessages.First(x => x.StartsWith("Discount amount $" + String.Format("{0:0.00}", discountAmount) + " per unit, qty " + origQty + ", for a total of $" + String.Format("{0:0.00}", Convert.ToDouble(LineLevelDiscountAmount)) + " for item " + itemNumber));
                    }
                    catch
                    {
                        Assert.Fail("'" + "Discount amount $" + String.Format("{0:0.00}", discountAmount) + " per unit, qty " + origQty + ", for a total of $" + String.Format("{0:0.00}", Convert.ToDouble(LineLevelDiscountAmount)) + " for item " + itemNumber + "' Message is not available in the following list \n" + String.Join("\n", list_InfologMessages));
                    }

                    Double ad_NetAmount = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Net amount"));
                    Assert.AreEqual(Math.Round(bd_NetAmount - Convert.ToDouble(LineLevelDiscountAmount), 2), ad_NetAmount, "Net amount for Item number '" + itemNumber + "' is not calculated properly after applying Line Level Discount Amount: $" + String.Format("{0:0.00}", Convert.ToDouble(LineLevelDiscountAmount)));
                    row_SOLine.SelectARecord(false, false);
                }

                //  3.6, Click Complete in action pane
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Complete");
                Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Modify"), "Modify Button is not visible.");

                //  3.7, Click Close in infolog
                list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
                Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

                //  4, Apply header level discount in % (positive) for the sales order
                //  4.1, Go to Sales order tab and Click on Modify in action pane
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Modify");
                Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Complete"), "Sale Order is not editable.");

                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery charge"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    String itemNumber = row_SOLine.GetColumnValueOfARecord("Item number");
                    Double origQty = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Orig qty"));
                    Double bd_NetAmount = Convert.ToDouble(row_SOLine.GetColumnValueOfARecord("Net amount"));
                    Double discountAmount = Math.Round((bd_NetAmount * (Convert.ToDouble(HeaderLevelDiscountPercentage) / 100) / origQty), 2);
                    Double total = Math.Round(bd_NetAmount * (Convert.ToDouble(HeaderLevelDiscountPercentage) / 100), 2);

                    list_ExpectedDiscountMessages.Add("Discount amount $" + String.Format("{0:0.00}", discountAmount) + " per unit, qty " + origQty + ", for a total of $" + String.Format("{0:0.00}", total) + " for item " + itemNumber);
                }

                //  4.2, Click Manual discounts => Sales order discount in action pane
                wnd_SO.ClickRibbonMenuItem("SalesOrder", "Manual discounts", "Sales order discount");

                //  4.3, Enter @HeaderLevelDisountPercentage in Manual Disount %, Select the @HeaderleveldisountReasoncode, Click on Apply and Close
                wnd_SODiscount = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order discount:", ClassName = "AxTopLevelFrame" });
                edit_DicountAmount = wnd_SODiscount.DetectControl<WinEdit>(new { Name = "Manual discount %" });
                edit_DicountAmount.EnterWinTextV2(HeaderLevelDiscountPercentage);
                wnd_SODiscount.SelectValueFromCustomDropdown("Reason code", "Default comment", HeaderLevelDiscountReasonCode);
                btn_ApplyAndClose = wnd_SODiscount.DetectControl<WinButton>(new { Name = "Apply and Close" });
                btn_ApplyAndClose.WinClick();

                //  4.4, Click Close in infolog
                list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
                Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

                foreach (String message in list_ExpectedDiscountMessages)
                {
                    try
                    {
                        String discountMessage = list_InfologMessages.First(x => x.StartsWith(message));
                    }
                    catch
                    {
                        Assert.Fail("'" + message + "' Message is not available in the following list \n" + String.Join("\n", list_InfologMessages));
                    }
                }

                //  4.5, Click Complete in action pane            
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.ClickRibbonMenuItem("SalesOrder", "Maintain", "Complete");
                Assert.IsNotNull(wnd_SO.FindRibbonMenuItem("Sales order", "Maintain", "Modify"), "Modify Button is not visible.");

                //  4.6, Click Close in infolog
                list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
                Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

                //  5, Select the main line in SO form Click Discount history (right side drop down)
                //  6, Check the discount history
                //  7, Click Close in Discount history page
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                //wnd_SO.ApplyColumnFilter("SalesLineGrid", "Line stage", "Is not", "Delivery charge");            
                uitcc_SOLines = wnd_SO.GetAllRecords("SalesLineGrid");
                uitcc_SOLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Delivery charge"));
                foreach (WinRow row_SOLine in uitcc_SOLines)
                {
                    row_SOLine.SelectARecord(true, false);
                    wnd_SO.ClickTableMenuItem("LineOverviewActionTab", "Discount history", null);

                    //  6,
                    wnd_DH = UITestControl.Desktop.FindWinWindow(new { Name = "Discount history:", ClassName = "AxTopLevelFrame" });
                    uitcc_DHLines = wnd_DH.GetAllRecords("ManualDiscounts");
                    uitcc_DHLines.RemoveAll(x => x.GetProperty("Value").ToString().Contains("Off"));

                    //Before
                    Double b_NetAmountPriorToDiscount = Convert.ToDouble(((WinRow)uitcc_DHLines[uitcc_DHLines.Count - 1]).GetColumnValueOfARecord("Net amount prior to discount"));
                    Double b_DiscountPercentage = Convert.ToDouble(((WinRow)uitcc_DHLines[uitcc_DHLines.Count - 1]).GetColumnValueOfARecord("Discount %"));
                    Double b_DiscountAmount = Convert.ToDouble(((WinRow)uitcc_DHLines[uitcc_DHLines.Count - 1]).GetColumnValueOfARecord("Discount $"));
                    String b_DiscountDescription = ((WinRow)uitcc_DHLines[uitcc_DHLines.Count - 1]).GetColumnValueOfARecord("Discount description");
                    Double b_NetAmountOfSalesLine = Convert.ToDouble(((WinRow)uitcc_DHLines[uitcc_DHLines.Count - 1]).GetColumnValueOfARecord("Net amount of sales line"));

                    Assert.AreEqual(0, b_DiscountPercentage, "Discount History Before Applying Discount: Discount % is " + b_DiscountPercentage);
                    Assert.AreEqual(0, b_DiscountAmount, "Discount History Before Applying Discount: Discount $ is " + b_DiscountAmount);
                    Assert.AreEqual("(null)", b_DiscountDescription, "Discount History Before Applying Discount: Discount Description is " + b_DiscountDescription);

                    //After Line Level
                    Double l_NetAmountPriorToDiscount = Convert.ToDouble(((WinRow)uitcc_DHLines[1]).GetColumnValueOfARecord("Net amount prior to discount"));
                    Double l_DiscountAmount = Convert.ToDouble(((WinRow)uitcc_DHLines[1]).GetColumnValueOfARecord("Discount $"));
                    String l_DiscountDescription = ((WinRow)uitcc_DHLines[1]).GetColumnValueOfARecord("Discount description");
                    Double l_NetAmountOfSalesLine = Convert.ToDouble(((WinRow)uitcc_DHLines[1]).GetColumnValueOfARecord("Net amount of sales line"));

                    Assert.AreEqual(l_NetAmountOfSalesLine + l_DiscountAmount, l_NetAmountPriorToDiscount, "Discount History After Applying Line Level Discount: Net amount prior to discount is " + l_NetAmountPriorToDiscount);
                    Assert.AreEqual(Convert.ToDouble(LineLevelDiscountAmount), l_DiscountAmount, "Discount History After Applying Line Level Discount: Discount $ is " + l_DiscountAmount);
                    Assert.AreEqual(LineLevelDiscountReasonCode, l_DiscountDescription, "Discount History After Applying Line Level Discount: Discount Description is " + l_DiscountDescription);
                    Assert.AreEqual(Math.Round((l_NetAmountPriorToDiscount - l_DiscountAmount), 2), l_NetAmountOfSalesLine, "Discount History After Applying Line Level Discount: Net amount of sales line is " + l_NetAmountOfSalesLine);

                    //After Header Level
                    Double h_NetAmountPriorToDiscount = Convert.ToDouble(((WinRow)uitcc_DHLines[0]).GetColumnValueOfARecord("Net amount prior to discount"));
                    Double h_DiscountPercentage = Convert.ToDouble(((WinRow)uitcc_DHLines[0]).GetColumnValueOfARecord("Discount %"));
                    String h_DiscountDescription = ((WinRow)uitcc_DHLines[0]).GetColumnValueOfARecord("Discount description");
                    Double h_NetAmountOfSalesLine = Convert.ToDouble(((WinRow)uitcc_DHLines[0]).GetColumnValueOfARecord("Net amount of sales line"));

                    Assert.AreEqual(l_NetAmountOfSalesLine, h_NetAmountPriorToDiscount, "Discount History After Applying Header Level Discount: Net amount prior to discount is " + h_NetAmountPriorToDiscount);
                    Assert.AreEqual(Convert.ToDouble(HeaderLevelDiscountPercentage), h_DiscountPercentage, "Discount History After Applying Header Level Discount: Discount % is " + h_DiscountPercentage);
                    Assert.AreEqual(HeaderLevelDiscountReasonCode, h_DiscountDescription, "Discount History After Applying Header Level Discount: Discount Description is " + h_DiscountDescription);
                    Assert.AreEqual(Math.Round((h_NetAmountPriorToDiscount - (h_NetAmountPriorToDiscount * h_DiscountPercentage / 100))),Math.Round(h_NetAmountOfSalesLine), "Discount History After Applying Header Level Discount: Net amount of sales line is " + h_NetAmountOfSalesLine);

                    //  7,
                    wnd_DH.CloseWindow();
                    row_SOLine.SelectARecord(false, false);
                }


                #endregion

                wnd_SO.CloseWindow();

                #region Test Cleanup

              
                #endregion
            }
        }


        [TestMethod]
        [Timeout(TestTimeout.Infinite)]
        [WorkItem(91908)]
        [Description("Address Maintenance")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.TestCase", "http://aarcwivpap10797:8080/tfs/Ashley;Ashley.QA", "91908", DataAccessMethod.Sequential)]
        public void AddressMaintenance()
        {
            int iterationCount = TestContext.DataRow.Table.Rows.IndexOf(TestContext.DataRow);
            if (iterationCount == 0)
            {
                #region TestData

                string PurchaseOrder = String.Empty;
                string Lastname = TestContext.DataRow["Lastname"].ToString();
                string Firstname = TestContext.DataRow["Firstname"].ToString();
                List<String> InfologWarningMessage1 = TestContext.DataRow["InfologWarningMessage1"].ToString().Split(';').ToList().Select(x => x.Trim()).ToList();
                List<String> InfologWarningMessage2 = TestContext.DataRow["InfologWarningMessage2"].ToString().Split(';').ToList().Select(x => x.Trim()).ToList();
                string InvoiceAddress1 = TestContext.DataRow["InvoiceAddress1"].ToString();
                string InvoiceZIPcode = TestContext.DataRow["InvoiceZIPcode"].ToString();
                List<String> InvoiceAddrValidationmsg = TestContext.DataRow["InvoiceAddrValidationmsg"].ToString().Split(';').ToList().Select(x => x.Trim()).ToList();
                string DeliveryAddress1 = TestContext.DataRow["DeliveryAddress1"].ToString();
                string DeliveryZIPcode = TestContext.DataRow["DeliveryZIPcode"].ToString();
                string InvoiceCounty = TestContext.DataRow["InvoiceCounty"].ToString();
                string InvoiceCity = TestContext.DataRow["InvoiceCity"].ToString();
                string InvoiceState = TestContext.DataRow["InvoiceState"].ToString();
                string DeliveryCounty = TestContext.DataRow["DeliveryCounty"].ToString();

                string Path = AX.Automation.Properties.Settings.Default.AX_Path;
                string AddressPath = AX.Automation.Properties.Settings.Default.AddressPath;

                #endregion

                #region Local Variables

                WinWindow wnd_SO = new WinWindow();
                WinWindow wnd_AddressMaintenance = new WinWindow();
                WinRow row_DeliveryAddress = new WinRow();
                WinButton btn_ApplyAndClose = new WinButton();
                WinClient client_FastTab = new WinClient();

                Dictionary<String, String> dict_ColumnAndValues = new Dictionary<String, String>();
                List<String> list_InfologMessages = new List<String>();
                String deliveryAddress = String.Empty;
                String headerViewAddress = String.Empty;
                String lineViewAddress = String.Empty;

                #endregion

                #region Pre-requisite
                //  1, Create Sales Order using credit card payment type
                saleOrder = "725000144596";
               

                #endregion

                #region Test Steps

                //  2, Open the Sales Order
                // 2.1, Go to ECM/Accounts receivable/Common/Sales orders/All sales orders                              
                wnd_AX = wnd_AX.LaunchDynamicsAX(Path);
                wnd_AX.NavigateTo(AddressPath);
                wnd_AX.WaitForSaleOrderToBeDisplayedInAX(saleOrder);

                // 2.2, Search using the SO confirmation number in the grid and double click on the created SO
                wnd_AX.SelectARecord("Grid", new Dictionary<string, string>() { { "Sales order", saleOrder } }, true, true);

                //  3, Click Sales order tab
                //  Click on Address maintenance => Address maintenance in action pane
                wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                wnd_SO.ClickRibbonMenuItem("Main", "Address maintenance group", "Address maintenance");

                //  4, Click Add button in Address tab
                wnd_AddressMaintenance = UITestControl.Desktop.FindWinWindow(new { Name = "‪Address maintenance:", ClassName = "AxTopLevelFrame" });
                client_FastTab = wnd_AddressMaintenance.ExpandFastTab("Addresses", true);
                //wnd_AddressMaintenance.ClickTableMenuItem("LogisticsPostalAddressActionPaneTab", "Add", null);

                ////  5, Enter no value in First name field
                ////  Enter @Lastname, Click on the next line in grid
                //dict_ColumnAndValues = new Dictionary<string, string>();
                //dict_ColumnAndValues.Add("Primary", "No");
                //dict_ColumnAndValues.Add("Purpose", "Delivery");
                //dict_ColumnAndValues.Add("Address 1", "(null)");
                //dict_ColumnAndValues.Add("City", "(null)");
                //dict_ColumnAndValues.Add("State", "(null)");

                //wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "Last name", Lastname);
                //Keyboard.SendKeys("{DOWN}");

                ////  6, Click Close in infolog
                //list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
                //foreach (String message in InfologWarningMessage1)
                //{
                //    try
                //    {
                //        String warningMessage = list_InfologMessages.First(x => x.StartsWith(message));
                //    }
                //    catch
                //    {
                //        Assert.Fail("'" + message + "' Message is not available in the following list \n" + String.Join("\n", list_InfologMessages));
                //    }
                //}

                ////  7, Enter @Firstname in new line
                ////  Enter no value in Last name field
                ////  Click on the next line in grid
                //wnd_AddressMaintenance.ClickTableMenuItem("LogisticsPostalAddressActionPaneTab", "Delete", null);
                //wnd_AddressMaintenance.ClickTableMenuItem("LogisticsPostalAddressActionPaneTab", "Add", null);
                //wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "First name", Firstname);
                //Keyboard.SendKeys("{DOWN}");

                //// 8, Click Close in infolog
                //list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
                //foreach (String message in InfologWarningMessage2)
                //{
                //    try
                //    {
                //        String warningMessage = list_InfologMessages.First(x => x.StartsWith(message));
                //    }
                //    catch
                //    {
                //        Assert.Fail("'" + message + "' Message is not available in the following list \n" + String.Join("\n", list_InfologMessages));
                //    }
                //}

                ////  9, Select/Highlight the new line
                ////  Click Delete in Addresses tab
                //wnd_AddressMaintenance.ClickTableMenuItem("LogisticsPostalAddressActionPaneTab", "Delete", null);
                //Assert.IsNull(wnd_AddressMaintenance.FetchARecordFromATable("GridLogisticsPostalAddress", dict_ColumnAndValues), "New Line is not removed from the grid.");

                //  10, Change the @InvoiceAddress1 value in Invoice line (with purpose = Invoice)
                //  Change the @InvoiceZIPcode value in Invoice line
                //  Click Address Validation in Address tab
                //dict_ColumnAndValues = new Dictionary<string, string>();
                //dict_ColumnAndValues.Add("Purpose", "Invoice");
                //wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "Address 1", InvoiceAddress1);

                //wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "ZIP/postal code", InvoiceZIPcode);

                //wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "City", InvoiceCity);

                //wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "State", InvoiceState);

                //wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "County", InvoiceCounty);

                //wnd_AddressMaintenance.ClickTableMenuItem("LogisticsPostalAddressActionPaneTab", "Address Validation", null);
                //list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
                //foreach (String message in InvoiceAddrValidationmsg)
                //{
                //    try
                //    {
                //        String warningMessage = list_InfologMessages.First(x => x.StartsWith(message));
                //    }
                //    catch
                //    {
                //        Assert.Fail("'" + message + "' Message is not available in the following list \n" + String.Join("\n", list_InfologMessages));
                //    }
                //}

                //  11, Change the @DeliveryAddress1 value in Deliivery line (with purpose = Delivery)
                //  Change the @DeliveryZIPcode valie in Delivery line
                //  //  Click Address Validation in Address tab
                //  Select the Sales order in Orders grid
                //  Click Apply and close in Address maintenance form
                //  Click close in infolog
                dict_ColumnAndValues = new Dictionary<string, string>();
                dict_ColumnAndValues.Add("Purpose", "Delivery");
                wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "Address 1", DeliveryAddress1);

                wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "ZIP/postal code", DeliveryZIPcode);

                wnd_AddressMaintenance.EnterValueToCell("GridLogisticsPostalAddress", dict_ColumnAndValues, "County", DeliveryCounty);
                Keyboard.SendKeys("{TAB}");

                wnd_AddressMaintenance.ClickTableMenuItem("LogisticsPostalAddressActionPaneTab", "Address Validation", null);
                list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);
                Assert.IsFalse(list_InfologMessages.Contains("One or more critical STOP errors have occurred. Use the error messages below to guide you or call your administrator."), String.Join("\n", list_InfologMessages));

                row_DeliveryAddress = wnd_AddressMaintenance.FetchARecordFromATable("GridLogisticsPostalAddress", dict_ColumnAndValues);
                deliveryAddress = row_DeliveryAddress.GetColumnValueOfARecord("Address 1") + "|" + row_DeliveryAddress.GetColumnValueOfARecord("City") + ", " + row_DeliveryAddress.GetColumnValueOfARecord("State") + "  " + row_DeliveryAddress.GetColumnValueOfARecord("ZIP/postal code") + "|" + row_DeliveryAddress.GetColumnValueOfARecord("Country/region");

                client_FastTab = wnd_AddressMaintenance.ExpandFastTab("Orders", true);
                wnd_AddressMaintenance.CheckInsideACell("GridSalesOrder", new Dictionary<string, string>() { { "Sales order", saleOrder } }, "Mark", true);

                btn_ApplyAndClose = wnd_AddressMaintenance.DetectControl<WinButton>(new { Name = "Apply and Close" });
                btn_ApplyAndClose.WinClick();

                list_InfologMessages = DynamicsAxCommonFunctions.HandleInfolog(true);

                ////  12, Go to the SO form - Header view
                ////  Click File =->Refresh
                ////  Expand Address fast tab and verify the Delivery address
                //wnd_SO = UITestControl.Desktop.FindWinWindow(new { Name = "Sales order:", ClassName = "AxTopLevelFrame" });
                //wnd_SO.ClickRibbonMenuItem("Main", "Show", "Header view");

                //wnd_SO.ChooseFromRibbonMenu("File", new List<string>() { "Refresh" });

                //client_FastTab = wnd_SO.ExpandFastTab("Address", true);
                //headerViewAddress = client_FastTab.DetectControl<WinEdit>(new { Name = "Address" }).Text;
                //headerViewAddress = new Regex("[" + Environment.NewLine + "]{2,}", RegexOptions.None).Replace(headerViewAddress, "|");
                //Assert.AreEqual(deliveryAddress, headerViewAddress, "'Delivery Address' entered in 'Address Maintenance Form' and 'Delivery Address' available in 'Header View-Address Fast Tab' of Sales Order Form are different.");

                ////  13, Click on Line view in action pane
                ////  Line details fast tab -> Address tab
                ////  verify the Delivery address
                //wnd_SO.ClickRibbonMenuItem("Main", "Show", "Line view");

                //client_FastTab = wnd_SO.ExpandFastTab("Line details", true);
                //client_FastTab.SelectTabInFastTab("Address");
                //client_FastTab.DetectControl<WinEdit>(new { Name = "Delivery address" }).WinHover();

                //lineViewAddress = UITestControl.Desktop.DetectControl<WinWindow>(new { Name = "EnhancedPreview" }).DetectControl<WinEdit>(new { Name = "Address" }).Text;
                //lineViewAddress = new Regex("[" + Environment.NewLine + "]{2,}", RegexOptions.None).Replace(lineViewAddress, "|");
                //Assert.AreEqual(deliveryAddress, lineViewAddress, "'Delivery Address' entered in 'Address Maintenance Form' and 'Delivery Address' available in 'Line View-Line details Fast Tab-Address Tab' of Sales Order Form are different.");
                //wnd_SO.CloseWindow();

                #endregion

                #region Test Cleanup


                #endregion
            }
        }

        #region Additional test attributes

        [TestInitialize()]
        public void MyTestInitialize()
        {
            
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestContext.CustomTestCleanup();
        }

        #endregion

        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
